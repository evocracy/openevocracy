image: node:10

services:
- docker:stable-dind

stages:
- build
- test
- release
- deploy

.env-rules:
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      variables:
        ENVIRONMENT: production
        ENVIRONMENT_SHORT: prod
    - if: $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH
      variables:
        ENVIRONMENT: development
        ENVIRONMENT_SHORT: dev

test_package_check:
  allow_failure: true
  script:
  - npm i npm-check
  - npm-check || true
  - pushd src
  - npm-check || true
  - popd

test_audit:
  allow_failure: true
  script:
  - npm i synode
  - synode .
  - npm audit || true
  - pushd src
  - npm audit || true
  - popd

test_sast:
  image: docker:stable
  stage: test
  variables:
    DOCKER_DRIVER: overlay2
  allow_failure: true
  script:
    - export SP_VERSION=$(echo "$CI_SERVER_VERSION" | sed 's/^\([0-9]*\)\.\([0-9]*\).*/\1-\2-stable/')
    - docker run
        --env SAST_CONFIDENCE_LEVEL="${SAST_CONFIDENCE_LEVEL:-3}"
        --volume "$PWD:/code"
        --volume /var/run/docker.sock:/var/run/docker.sock
        "registry.gitlab.com/gitlab-org/security-products/sast:$SP_VERSION" /app/bin/run /code
  artifacts:
    paths: [gl-sast-report.json]

test_platform:
  image: ubuntu:bionic
  stage: test
  variables:
    DOCKER_DRIVER: overlay2
    DOCKER_HOST: tcp://docker:2375/
  allow_failure: true
  script:
  - apt-get update
  - apt-get install -y curl wget
  - curl -fsSL https://get.docker.com -o get-docker.sh
  - sh get-docker.sh
  - usermod -aG docker $(whoami)
  - curl -L "https://github.com/docker/compose/releases/download/1.23.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
  - chmod +x /usr/local/bin/docker-compose
  - curl -sL https://deb.nodesource.com/setup_8.x | bash -
  - curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
  - echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
  - apt-get update
  - apt-get install -y nodejs yarn build-essential
  - cp shared/config.base.default.js shared/config.base.js
  - PORT=8080 docker-compose -f docker-compose.dev.yml up &
  - sleep 1m
  - docker-compose -f docker-compose.dev.yml down

test_integration:
  stage: test
  when: manual
  script:
  - cp shared/config.base.default.js shared/config.base.js
  - npm install
  - npm run test_integration

release_docker:
  image: docker:stable
  stage: release
  script:
  - docker login -u openevocracy -p $DOCKERHUB_ACCESS_TOKEN
  - docker build -t openevocracy/openevocracy:$CI_COMMIT_REF_SLUG .
  - docker push openevocracy/openevocracy:$CI_COMMIT_REF_SLUG

deploy:
  image: docker/compose:1.29.2
  stage: deploy
  extends: .env-rules
  environment:
    name: ${ENVIRONMENT}
    url: https://${ENVIRONMENT_SHORT}.openevocracy.org
  script:
    - apk add openssh

    - mkdir ~/.ssh
    - echo $SSH_PRIVATE_KEY | base64 -d > ~/.ssh/id_rsa
    - chmod -R 0700 ~/.ssh
    - ssh-keygen -f ~/.ssh/id_rsa -y > ~/.ssh/id_rsa.pub

    - mkdir ~/.docker
    - echo "{\"auths\":{\"${CI_REGISTRY}\":{\"auth\":\"$(printf "%s:%s" "${CI_REGISTRY_USER}" "${CI_REGISTRY_PASSWORD}" | base64 | tr -d '\n')\"},\"$CI_DEPENDENCY_PROXY_SERVER\":{\"auth\":\"$(printf "%s:%s" ${CI_DEPENDENCY_PROXY_USER} "${CI_DEPENDENCY_PROXY_PASSWORD}" | base64 | tr -d '\n')\"}}}" > ~/.docker/config.json
    - export DOCKER_HOST=$SSH_CONNECTION_STRING

    - echo "HOST *" > ~/.ssh/config
    - echo "StrictHostKeyChecking no" >> ~/.ssh/config

    - export BASE_URL=${ENVIRONMENT_SHORT}.openevocracy.org
    - export COMPOSE_PROJECT_NAME=openevocracy_${ENVIRONMENT_SHORT}
    - docker-compose pull
    - docker-compose up -d
    - docker network connect bridge ${COMPOSE_PROJECT_NAME}_web_1 || true
